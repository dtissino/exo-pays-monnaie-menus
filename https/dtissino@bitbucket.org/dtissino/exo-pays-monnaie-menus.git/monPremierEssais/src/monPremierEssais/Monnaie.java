package monPremierEssais;

import java.util.ArrayList;

public class Monnaie {
	
	/*propri�t� de l'objet de classe Monnaie
	 * 
	 */
	private String name;
	private char symbol;
	private float currencyGold;
	
	/*
	 * liste des monnaies
	 * static 
	 */
	public static ArrayList<Monnaie> monnaies = new ArrayList<Monnaie>();
	
	/*
	 * constructeur
	 */
	public Monnaie(String name, char symbol, float currencyGold)
	{
		this.name = name;
		this.symbol = symbol;
		this.currencyGold = currencyGold;
		
		monnaies.add(this);
	}
	
	//class method
	//set&get
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setSymbol(char symbol)
	{
		this.symbol = symbol;
	}
	
	public void setCurrencyGold(float currencyGold)
	{
		this.currencyGold = currencyGold;
	}
	
	//
	public String getName()
	{
		return this.name;
	}
	
	public char getSymbol()
	{
		return this.symbol;
	}
	
	public float getCurrencyGold()
	{
		return this.currencyGold;
	}
	
	/*calcul du taux de change entre deux instance de Monnaie, le calcul se fait � partir d'un objet Monnaie en cours, se comparant avec une autre instance 
	 * pass�e en parametre
	 * @param monnaie de classe Monnaie
	 */
	public float currencyCalculation(Monnaie monnaie)
	{		
		return (this.currencyGold/monnaie.currencyGold);
	}

}
