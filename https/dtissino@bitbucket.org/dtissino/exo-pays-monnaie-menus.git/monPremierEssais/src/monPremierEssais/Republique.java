package monPremierEssais;

public class Republique extends Pays{

	/*
	 * propriétés
	 * 
	 */
	private String partiGagnant;
	private String president;
	
	public Republique(String name, float superficie, int population, Monnaie monnaie, String partiGagnant, String president) 
	{
		super(name, superficie, population, monnaie);
		this.partiGagnant = partiGagnant;
		this.president = president;
	}
	
	public String toString()
	{
		return super.toString() + "\n le parti gagnant est" + this.partiGagnant + " son dirigeant s'apelle : " + this.president;
	}
}
