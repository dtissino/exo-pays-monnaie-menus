package monPremierEssais;

import java.util.ArrayList;

public class Pays {
	
	/*
	 * propri�t�s
	 */
	private String name;
	private float superficie;
	private int population;
	private Monnaie monnaie;
	
	/*
	 * liste des pays instanci�s
	 */
	public static ArrayList<Pays> listPays = new ArrayList<Pays>();
	
	/*
	 * constructeur
	 *
	 */
	public Pays(String name, float superficie, int population, Monnaie monnaie)
	{
		this.name = name;
		this.superficie = superficie;
		this.population = population;
		this.monnaie = monnaie;
		
		listPays.add(this);
	}
	
	/*
	 * methodes de classes
	 * set&get
	 */
	public void setName(String name)
	{
		this.name = name;
	}
	
	public void setSuperficie(float superficie)
	{
		this.superficie = superficie;
	}
	
	public void setPopulation(int population)
	{
		this.population = population;
	}
	
	public void setMonnaie(Monnaie monnaie)
	{
		this.monnaie = monnaie;
	}
	
	//
	public String getName()
	{
		return this.name;
	}
	
	public float getSuperficie() 
	{
		return this.superficie;
	}
	
	public int getPopulation() 
	{
		return this.population;
	}
	//TODO don't really need?
	public Monnaie getMonnaie() 
	{
		return this.monnaie;
	}
	
	//
	public String toString()
	{
		return "\n \t" + "pays nomm� " + this.name + "de " + this.population + " habitants, d'une superficie de " + this.superficie + " km�, avec le" + this.monnaie + " comme monnaie.";
	}
	
	/*
	 * description to string de l'instance en cours + une instance en parametre et affiche le taux de change de celles ci
	 * @param p de classe Pays
	 */
	
	public void comparerPays(Pays p)
	{
		try
		{
			this.toString();
			System.out.println("\n \t \t--------------------------------");
			p.toString();
			System.out.println(this.name + "poss�de un taux de change de " + this.monnaie.currencyCalculation(p.monnaie) + "par rapport � la monnaie de " + p.name + "\n \n");
		}
		catch(NullPointerException e)
		{
			System.out.println("aucune comparaison n'est pour le moment possible.");
		}

	}
	
	
}
