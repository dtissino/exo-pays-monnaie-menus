package monPremierEssais;

public class Royaume extends Pays{

	/*
	 * propriétés
	 * 
	 */
	private String souverain;
	private String palais;
	
	public Royaume(String name, float superficie, int population, Monnaie monnaie, String souverain, String palais) 
	{
		super(name, superficie, population, monnaie);
		this.souverain = souverain;
		this.palais = palais;
	}
	
	public String toString()
	{
		return super.toString() + "\n le dirigeant est " + this.souverain + " et le palais est : " + this.palais;
	}

}
