package monPremierEssais;

import java.util.Scanner;

/*
 * class appelante
 * note perso, �a fait longtemps que j'ai plus c�d�, trop longtemps, j'ai l'impression d'avoir oubli� toutes les bons placements/conventions ou autres
 */


/*
 * DISSCLAMER !!!!!!!!!!! LA METHODE REFAITE TOSTRING DANS LA CLASSE PAYS NE SE LANCE PAS J AI ABANDONNE LA RECHERCHE J AI JUSTE ENVIE DE PIEUTER
 * JE RELIRAI CA CE SOIR !
 */
public class Essais {

	public static void main(String[] args) {
		
		/*
		 * cr�ation des objets menus
		 */
		Menu mainMenu = new Menu();
		Menu monnaieMenu = new Menu();
		Menu paysMenu = new Menu();
		
		//initialisation du main menu
		mainMenu.add("Ajouter une Monnaie");
		mainMenu.add("Ajouter un Pays");
		mainMenu.add("Comparer deux Pays");
		mainMenu.add("Quitter");
		
		//bouclage du menu principal
		boolean loop = true;
		
		while (loop) 
		{
			//structure switch sans default (vu que le cas est trait� dans menu + coh�rant en terme de "menus")
			switch (mainMenu.choose()) 
			{
			// ajouter monnaie
			case 1 : 
				//appel de la methode
				preBuilder(monnaieMenu);
				
				break;
				
			// ajouter pays
			case 2 : 
				if (!Monnaie.monnaies.isEmpty()) 
				{
					//d�claration (c & scanner) dans un switch ... ok?
					char c;
					Scanner sc = new Scanner(System.in);
					
					System.out.println(" Republique(r) ou Royaume(k) : ");
					c = sc.nextLine().charAt(0);
					
					//verification rapide de l'entree pour la selection de l'entree republique ou royaute
					while (c != 'r' && c != 'k') 
					{	 
						System.out.println("Saisie incorrecte, veuillez recommencer (k/r) : ");
						c = sc.nextLine().charAt(0);	
					} 
					
					System.out.println("Choix de la monnaie : ");
					
					//appel de la methode
					preBuilder(monnaieMenu.choose()-1, c, paysMenu);
				} 
				else 
				{
					System.out.println("La liste de Monnaie est pour le moment vide, veuillez d'abord la remplir avant de proc�der � l'ajout d'un pays !");
				}
				
				break;
				
			//comparaison de pays
			case 3 :
				Pays p = null;
				
				//test des cas possibles
				//besoin de try catch?
				if (!(Pays.listPays.size() == 1 || Pays.listPays.isEmpty())) //(Pays.listPays.size() <= 1) empty verifie le cas liste null
				{
					System.out.println("Selectionner les pays � comparer : \n numero 1 : ");
					p = Pays.listPays.get(paysMenu.choose()-1);
					System.out.println("Selectionner les pays � comparer : \n numero 2 : ");
					p.comparerPays(Pays.listPays.get(paysMenu.choose()-1));
					
				} 
				else 
				{
					System.out.println("la liste doit au moins comporter deux Pays pour qu'une comparaison soit possible");
				}
				
				
				break;
				
			//quitter
			case 4 : loop = false;
				
				break;
			}
		}
	}
	
	/*
	 * Permet la saisie des donn�es n�c�ssaire � l'instanciation de la classe Monnaie, le parametre de conversion d'or est test� sur sa nature de nombre ainsi
	 * que sa sup�riorit� � 0, aucun retour, ajoute un champ � la liste menu des monnaies pass�e en parametre
	 * n'ayant pas trouv� de moyen correct d'actualiser les listes menu et pays je les passes en param de methode	 
	 * @param monaieMenu permet la mise � jour de la liste monnaieMenu
	 */
	static void preBuilder(Menu monnaieMenu)
	{
		String pName;
		float pCurrency = 0.0f;
		char pSymbol;
		boolean loop = true;
		Scanner sc = new Scanner(System.in);
		
		//affichage&saisie
		//scanner � la bourine, rien de plus opti?
		System.out.println("entrez le nom de la monnaie : ");
		pName = sc.nextLine();
		System.out.println("entrez le symbole de la monnaie : ");
		pSymbol = sc.nextLine().charAt(0);
		System.out.println("entrez le taux de change a l'or : ");
		
		//� consid�rer qu'aucun pays n'aie "l'or" en tant que monnaie nationale
		// encore une fois les conditions paraissent barbare, j'ai but� pendant un moment dessus, tournant en rond la plupart du temps, j'aurai besoin de clart�
		// sur ce qui est bon � faire
		//la boucle tourne tant que le cas du parsing en float n'est pas termin�, elle v�rifie aussi que la valeur soit sup�rieure � 0
		while(loop || pCurrency <= 0)
		{	
			try 
			{
				pCurrency = Float.parseFloat(sc.nextLine());
				loop = false;
			} 
			catch (NumberFormatException e) 
			{
				loop = true;
			}
			if (loop || pCurrency <= 0) 
			{	
				System.out.println("erreur de format, recommencez l'encodage : ");
			}
		}
		
		//appel du constructeur de Monnaie en lui passant les entr�es r�colt�es
		Monnaie monnaie = new Monnaie (pName, pSymbol, pCurrency);	
		
		//ajout � la liste
		monnaieMenu.add(monnaie.getName());
		
	}
	
	/*
	 * surcharge de la methode preBuilder servant ici � l'instanciation de la classe Pays, le parametre de superficie et de nombre d'habitant sont test� sur leur
	 * nature de nombre ainsi qu'� leur sup�riorit� � 0, aucun retour
	 * n'ayant pas trouv� de moyen correct d'actualiser les listes menu et pays je les passes en param de methode
	 * @param choice nombre associ� au choix de l'utilisateur concernant la liste de monnaie pr�sent�e
	 * @param c caract�re associ� au choix de l'utilisateur concernant la nature de la sous classe � la classe Pays
	 * @param paysMenu permet de mettre � jour la liste des pays du menu
	 */
	static void preBuilder(int choice, char c, Menu paysMenu)
	{
		Scanner sc = new Scanner(System.in);
		//note je ne sais pas si je dois initialiser les objets en attente � NULL ou pas
		Pays p = null;
		String pName;
		float pSuperficie = 0.0f;
		int pPopulation = 0;
		boolean loop = true;
		
		//affichage&saisie
		System.out.println("entrez le nom : ");
		pName = sc.nextLine();
		System.out.println("entrez la superficie (km�) : ");
		
		//m�me boucle limite copi� coll� de la methode pr�c�dente
		//la boucle tourne tant que le cas du parsing en float n'est pas termin�, elle v�rifie aussi que la valeur soit sup�rieure � 0
		while (loop || pSuperficie <= 0) 
		{
			try 
			{
				pSuperficie = Float.parseFloat(sc.nextLine());
				loop = false;
			}
			catch (NumberFormatException e) 
			{
				loop = true;
			}
			
			if(loop || pSuperficie <= 0)
			{
				System.out.println("erreur de format, recommencez l'encodage : ");
			}
		}
		
		System.out.println("entrez la population : ");
		
		//m�me boucle ... je sais pas trop
		//la boucle tourne tant que le cas du parsing en integer n'est pas termin�, elle v�rifie aussi que la valeur soit sup�rieure � 0
		loop = true;
		while(loop || pPopulation <= 0)
		{
			try
			{
				pPopulation = Integer.parseInt(sc.nextLine());
				loop = false;
			}
			catch(NumberFormatException e)
			{
				loop = true;
			}
			
			if(loop || pPopulation <= 0)
			{
				System.out.println("erreur de format, recommencez l'encodage : ");
			}
		}
		
		//initialisation ...?  du champ monnaie gr�ce � la methode get qui permet d'aller chercher un element d'index entier E d'une liste
		Monnaie monnaie = Monnaie.monnaies.get(choice);
		
		// en y pensent l� tout de suite, vu que les deux param additionnel sont des strings, j'aurai sans doute plus facile de d�clarer plus haut deux variables
		//strings pRandomNom1 et pRandomNom2, en fait je suis m�me pas sur qu'un switch pour un tel cas soit vraiment propre � ce niveau ... je fatigue s�rieux 
		// j'aurai du aller pieuter plus t�t, �a a l'air tellement d�geu comme code
		switch (c) 
		{
		case 'k' :
			//royaume
			String pNameDirigeant;
			String pPalais;
			
			System.out.println("nom du dirigeant : ");
			pNameDirigeant = sc.nextLine();
			System.out.println("nom du monument : ");
			pPalais = sc.nextLine();
			
			//new objet royaume cast from Pays p
			p = new Royaume(pName, pSuperficie, pPopulation, monnaie, pNameDirigeant, pPalais);
			
			break;
		case 'r' :
			//republique
			String pPartiGagnant;
			String pPresident;
			
			System.out.println("nom du parti politique gagnant : ");
			pPartiGagnant = sc.nextLine();
			System.out.println("nom du president : ");
			pPresident = sc.nextLine();
			
			//new objet republique cast from Pays p
			p = new Republique(pName, pSuperficie, pPopulation, monnaie, pPartiGagnant, pPresident);
			
			break;
		}
		
		//ajout du pays cr�e � la liste du menu & test
		try
		{
			paysMenu.add(p.getName());
		}
		catch(NullPointerException e)
		{
			System.out.println("objet null, annulation de la mise en liste");
		}
		
		
	}
}
