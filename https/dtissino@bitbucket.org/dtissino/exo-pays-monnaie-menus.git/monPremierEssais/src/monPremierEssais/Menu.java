package monPremierEssais;

import java.util.ArrayList;
import java.util.Scanner;

/*
 * Cette classe permet de cr�er un menu, de l'afficher � l'�cran et de demander � l'utilisateur de choisir une entr�e
 * @author Maxime Pardoen
 */
public class Menu {
	ArrayList<String> elements = new ArrayList(); 

	public Menu(){
		
	}
	
	/*
	 * Ajoute une nouvelle entr�e � la fin du menu
	 * @param newEntry Contenu de la nouvelle entr�e
	 * @return renvoie la taille actuelle du menu
	 */
	public int add(String newEntry){
		elements.add(newEntry);
		return elements.size();
	}
	
	/*
	 * Affiche le menu et demande � l'utilisateur de choisir une entr�e
	 * @return renvoie le numero de l'entr�e choisie par l'utilisateur
	 */
	public int choose(){
		for (int i = 0;i<elements.size();i++){
			System.out.println("["+(i+1)+"] "+elements.get(i));
		}
		int choice = 0;
		while (choice==0){
			Scanner sc = new Scanner(System.in);
			System.out.print("Choix [1-"+elements.size()+"]: ");
			String str = sc.nextLine();
			try{
				choice = Integer.parseInt(str);
			}
			catch(NumberFormatException e){
				choice = 0;
			}
			if (choice<1 || choice>elements.size()){
				choice = 0;
				System.out.println("Choix incorrect!");
			}

		}
		return choice;
	}
	
	/*
	 * Renvoie l'entr�e associ�e � un num�ro
	 * @param i numero associ�e � l'entr�e � afficher
	 * @return renvoie l'entr�e de num�ro i
	 * @throws IndexOutOfBoundsException
	 */
	public String get(int i){
		return elements.get(i-1);
	}

}